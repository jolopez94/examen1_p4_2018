#include "expr_lexer.h"

Token ExprLexer::getNextToken() {
    text = "";
    int cont = 0;
    while (1) {
        if (currSym == ' ' || currSym == '\t' || currSym == '\n') {
            getNextSymbol();
            continue;
        }

        switch (currSym) {
            case '+': 
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::OpAdd;
            case '*':
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::OpMul;
            case '-':
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::OpAdd;
            case '(':
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::OpenPar;
            case ')':
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::ClosePar;
            case ';':
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::Semicolon;
            case EOF:
                text = currSym;
                //std::cout << currSym << std::endl;
                getNextSymbol();
                return Token::Eof;
            default:
                if (isdigit(currSym)) {
                    while (isdigit(currSym) || currSym == '.') {
                        text += currSym;
                        //std::cout << currSym << std::endl;
                        getNextSymbol();
                    }
                    return Token::Num;
                } else if (isalpha(currSym) || currSym == '_') {
                    while (isalpha(currSym) || currSym == '_' || isdigit(currSym)) {
                        text += currSym;
                        //std::cout << currSym << std::endl;
                        getNextSymbol();
                    }
                    return Token::Id;
                } else if (currSym == '#') {
                    //std::cout << currSym << std::endl;
                    getNextSymbol();
                    if (currSym == '!') {
                        //std::cout << currSym << std::endl;
                        getNextSymbol();
                        bool consume = true;
                        while (consume){
                            if (currSym == '!') {
                                getNextSymbol();
                                if (currSym == '#') {
                                    getNextSymbol();
                                    consume = false;
                                } else {
                                    getNextSymbol();
                                }
                            } else {
                                getNextSymbol();
                            }
                            //std::cout << currSym << std::endl;
                        }
                        continue;
                    } else {
                        while (currSym != '\n' || currSym != EOF) {
                            if (currSym == EOF) {
                                text = currSym;
                                //std::cout << currSym << std::endl;
                                getNextSymbol();
                                return Token::Eof;
                            }
                            //std::cout << currSym << std::endl;
                            getNextSymbol();
                        } 
                    }
                } else {
                    throw "Error Unknow SYmbol";
                }
        }
        // cont ++;
        // if (cont == 20) {
        //     break;
        // }
    }
}