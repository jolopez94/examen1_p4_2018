#include "expr_parser.h"


void Parser::Expr() {
    Term();
    while (token == Token::OpAdd || token == Token::OpSub) {
        std::cout << lexer.getText() << std::endl;
        token = lexer.getNextToken();
        Term();
    }
}

void Parser::Term() {
    Factor();
    while (token == Token::OpMul || token == Token::OpDiv) {
        std::cout << lexer.getText() << std::endl;
        token = lexer.getNextToken();
        Factor();
    }
}

void Parser::Factor() {
    if (token == Token::Number) {
        std::cout << lexer.getText() << std::endl;
        token = lexer.getNextToken();
    } else if (token == Token::OpenPar) {
        std::cout << lexer.getText() << std::endl;
        token = lexer.getNextToken();
        Expr();
        if (token == Token::ClosePar) {
            std::cout << lexer.getText() << std::endl;
            token = lexer.getNextToken();
        } else {
            throw "Error";
        }
    } else {
        throw "Error";
    }
}

int Parser::parse() {
    token = lexer.getNextToken();
    std::cout << lexer.getText() << std::endl;
    Expr();
    int cont = 1;
    while (token == Token::Semicolon) {
        token = lexer.getNextToken();
        Expr();
        cont ++;
    }
    if (token == Token::Eof) {
        std::cout << "Parased Successfully" << std::endl;
    }else {
        throw "Error";
    }
    

    return cont;
}
